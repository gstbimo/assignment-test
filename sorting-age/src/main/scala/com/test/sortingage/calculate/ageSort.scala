package com.test.sortingage.calculate

import com.test.sortingage.model.sortedAge
import scala.io.Source

object ageSort {

  val filename = "src/main/resource/age.txt"
  val ages = Source.fromFile(filename).getLines.toArray

  def main(args: Array[String]) = {
//    val a = Array(23, 51, 35, 5, 3, 2, 1, 25, 46, 17, 39 ,369)
    val age = ages.map(_.toInt)
    sort(age).foreach(n=> (println(n)))
  }

  /**
    *
    * @param age
    * @return [[Array]] of [[sortedAge]]
    */
  def sort(age:Array[Int]): Array[Int] =  {
    if (age.length < 2) age
    else {
      val pivot = age(age.length / 2)
      sort (age filter (pivot>)) ++ (age filter (pivot == )) ++
        sort (age filter(pivot <))
    }
  }

}
