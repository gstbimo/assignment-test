from data import *
import collections
import sys
from datetime import *

id_user = int(sys.argv[1])


file1 = "/home/gustibimo/workspaces/salestock/user_preference.tsv"
file2 = "product_score.tsv" 
def intialize(user_preference_path,product_score_path):
    result_dict = {}
    user_dict={}
    product_dict={}
    
    today = date.today()
    with open(user_preference_path) as f:  
        for line in f:
            pid_dict = {}
            column = line.strip().split("   ")
            difftime = date.fromtimestamp(float(column[3])) - today
            #count user preference uid-pid effective score
            escore = round(float(column[2]) * 0.95 ** difftime.days,3)
            pid_dict[column[1]] = escore
            user_dict[column[0]] = pid_dict
    
            
    with open(product_score_path) as f:
        for line in f:
            name = line.strip().split("   ")
            product_dict[name[0]] = name[1]   
    result_dict["user_p"] = user_dict
    result_dict["product"] = product_dict
    return result_dict
            

def top5(uid):
    data = preference
    user_pref = data["user_p"][uid]
    hdict={}
    for pid,escore in user_pref.items():
        pscore = data["product"][pid]
        rscore = pscore * escore + pscore
        hdict[rscore]=pid
        od = collections.OrderedDict(sorted(hdict.items(), reverse=True))
    result = [pid for score,pid in od.items()]
    return "\n".join(str(r) for r in result)

print(top5(id_user))

print(intialize(file1,file2))   